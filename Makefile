UID := $(shell id -u)
GID := $(shell id -g)
CURRENT_PATH := $(shell pwd)
CURRENT_APP := $(shell basename $(CURRENT_PATH) | sed "s/\-//g" | sed "s/\_//g")
DB_PASSWORD := $(shell tr -dc "a-zA-Z0-9_*+-" < /dev/urandom | head -c "16")
DB := db # the db container name
WP := wordpress # the wordpress container name
NETWORK := $(CURRENT_APP)_default
DATETIME := $(shell date '+%Y-%m-%d-%H-%M-%S')

HELP_FUN = \
	%help; \
	while(<>) { \
		if(/^([a-z0-9_-]+):.*\#\#(?:@(\w+))?\s(.*)$$/) { \
			push(@{$$help{$$2 // 'options'}}, [$$1, $$3]); \
		} \
	}; \
	print "usage: make [target]\n\n"; \
	for ( sort keys %help ) { \
		print "$$_:\n"; \
		printf("\033[36m  %-20s %s\033[0m\n", $$_->[0], $$_->[1]) for @{$$help{$$_}}; \
		print "\n"; \
	}

# If the first argument is "composer"...
ifeq (composer,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "composer"
  COMPOSER_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
endif

.PHONY: help build rebuild start stop clean install
.DEFAULT: all

help:
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

all: build start ##@target Launch project with build then start

docker-compose.override.yml:
	@sed -e "s/{USER_ID}/$(UID)/g" \
		-e "s/{DB_PASSWORD}/$(DB_PASSWORD)/g" \
		docker-compose.override.sample.yml > docker-compose.override.yml

build: docker-compose.override.yml install ##@target Build project (build)
	docker-compose build

rebuild: docker-compose.override.yml install ##@target Rebuild project (build --no-cache)
	docker-compose build --no-cache --force-rm

start: docker-compose.override.yml ##@target Start all containers (up -d)
	docker-compose up -d

stop: ##@target Stop all containers (stop)
	docker-compose stop

restart: stop start ##@target Restart all containers (stop start)

clean: stop ##@target Clean all volume content (down -v) an delete docker-compose.override.yml
	docker-compose down -v
	-rm docker-compose.override.yml
	-rm app/vendor -rf
	-rm app/web/wp -rf

# Composer targets

install: ##@composer Run composer install to download plugins & themes dependencies
	docker run --rm --volume $(CURRENT_PATH)/app:/app --user=$(UID):$(GID) composer install

update: ##@composer Run composer update
	docker run --rm --volume $(CURRENT_PATH)/app:/app --user=$(UID):$(GID) composer update

composer: ##@composer Run composer
	docker run --rm -it --volume $(CURRENT_PATH)/app:/app --user=$(UID):$(GID) composer $(COMPOSER_ARGS)

## Specific Wordpress targets

wpshell: ##@wordpress Enter in running wordpress container with bash
	docker-compose exec --user=$(UID):$(GID) $(WP) bash

#wpcli: ##@wordpress Run a wordpress cli command passed to wp executable [CMD="post list"]
#	docker-compose exec --user=$(UID):$(GID) $(WP) /var/www/vendor/wp-cli/wp-cli/bin/wp

#wphost: ##@wordpress Search and remplace old hostname by new hostname [OLD="www.old.com" NEW="www.new.com"]
#	docker run $(WPCLI_PARAMS) $(WPCLI_IMAGE) wp search-replace '$(OLD)' '$(NEW)' --skip-columns=guid

## Database targets

adminer: ##@database Launch adminer on port 8080 linked to current app network (host = db)
	docker run --rm --net $(NETWORK) -p 8080:8080 --name adminer adminer

dbdump: ##@database Dump database from db container in sql/ directory
	docker-compose exec $(DB) sh -c 'mysqldump -h $(DB) -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD" "$$MYSQL_DATABASE"' > sql/dump/$(DATETIME).sql

dbimport: ##@database Import a database into db container [DUMP="sql/dump.sql"] ... not working
	docker-compose exec $(DB) sh -c 'mysql -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD" "$$MYSQL_DATABASE"' < $(DUMP)
