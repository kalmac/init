FROM registry.gitlab.com/hostinauvergne/rakija:apache2.4-php7.3

COPY --chown=www-data:www-data app /var/www/html/
COPY php/90-custom.ini /usr/local/etc/php/conf.d/
